-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 18 Janvier 2017 à 21:39
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `iut_tds`
--

-- --------------------------------------------------------

--
-- Structure de la table `prestation_rating`
--

CREATE TABLE `prestation_rating` (
  `id` int(11) NOT NULL,
  `prestation_id` int(11) NOT NULL,
  `user_ip` varchar(15) NOT NULL,
  `note` int(11) NOT NULL COMMENT 'note/5',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `prestation_rating`
--
ALTER TABLE `prestation_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prestation_id` (`prestation_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `prestation_rating`
--
ALTER TABLE `prestation_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `prestation_rating`
--
ALTER TABLE `prestation_rating`
  ADD CONSTRAINT `prestation_rating_ibfk_1` FOREIGN KEY (`prestation_id`) REFERENCES `prestation` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
