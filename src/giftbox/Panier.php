<?php
namespace giftbox;

use \giftbox\models\Prestation;

class Panier {
	public static $saved_cart;
	
	public static function add($id) {
		if ((self::$saved_cart !== null) && (!self::hasItem($id))) {
			self::$saved_cart[$id] = array(
				'q' => 1
			);
			self::update();
			return true;
		}
		return false;
	}
	
	public static function remove($id) {
		if ((self::$saved_cart !== null) && (self::hasItem($id))) {
			// remove the item from the array
			unset(self::$saved_cart[$id]);
			// delete cookie value
			unset($_COOKIE['cart']);
			// empty value and expiration one hour before
			setcookie('cart_items_cookie', '', time() - 3600);
			self::update();
			return true;
		}
		return false;
	}
	
	public static function hasItem($id) {
		return array_key_exists($id, self::$saved_cart);
	}
	
	public static function getItems() {
		$ids = array();
		foreach(self::$saved_cart as $id=>$name){
			array_push($ids, filter_var($id, FILTER_VALIDATE_INT));
		}
		return Prestation::find($ids);
	}
	
	public static function read() {
		self::$saved_cart = json_decode(stripslashes(isset($_COOKIE['cart']) ? $_COOKIE['cart'] : ''), true);
		self::$saved_cart = self::$saved_cart == null ? array() : self::$saved_cart; // to prevent null value
	}
	
	private static function update() {
		$json = json_encode(self::$saved_cart, true);
		setcookie('cart', $json, time() + (86400 * 30), '/'); // 86400 = 1 day
		$_COOKIE['cart'] = $json;
	}
	
	public static function getMontant() {
		$prixTotal = 0;
		$items = self::getItems();
		if (count($items) > 0) {
			foreach($items as $it) {
				$itcount = self::$saved_cart[$it->id]['q'];
				$ittot = number_format($it->prix * $itcount, 2);
				$prixTotal += $ittot;
			}
		}
		$prixTotal = number_format($prixTotal, 2);
		return $prixTotal;
	}
}