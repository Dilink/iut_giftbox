<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 06/12/2016
 * Time: 11:39
 */
namespace giftbox\models;

class Categorie extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'categorie';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public static function getAsArray() {
		$catsList = array();
		$cats = Categorie::select()->get();
		foreach ($cats as $c) {
			$catsList[$c->id] = $c->nom;
		}
		return $catsList;
	}
}