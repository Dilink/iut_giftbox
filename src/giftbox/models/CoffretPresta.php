<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 06/12/2016
 * Time: 11:39
 */
namespace giftbox\models;
class Coffret extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'coffretPresta';
    protected $primaryKey = 'id_coffret';
    public $timestamps = false;

	public function coffret() {
		return $this->belongsTo('\giftbox\models\Coffret', 'id_coffret');
	}
}