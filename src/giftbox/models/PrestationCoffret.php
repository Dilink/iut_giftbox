<?php
namespace giftbox\models;

class PrestationCoffret extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'prestation_coffret';
    protected $primaryKey = 'idLiaison';
    public $timestamps = false;
	
	public function prestations() {
		return $this->hasMany('\giftbox\models\Prestation', 'id');
	}
}