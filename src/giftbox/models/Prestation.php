<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 06/12/2016
 * Time: 11:39
 */
namespace giftbox\models;
class Prestation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = false;

	public function categorie() {
		return $this->belongsTo('\giftbox\models\Categorie', 'cat_id');
	}
}