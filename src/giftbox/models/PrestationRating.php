<?php
namespace giftbox\models;

class PrestationRating extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'prestation_rating';
    protected $primaryKey = 'id';
}

/*
ALTER TABLE prestation_rating
ADD FOREIGN KEY (`prestation_id`)
REFERENCES prestation(id);
*/