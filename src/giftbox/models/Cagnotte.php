<?php
namespace giftbox\models;

class Cagnotte extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'cagnotte';
    protected $primaryKey = 'id';
    public $timestamps = false;
}