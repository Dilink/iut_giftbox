<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 06/12/2016
 * Time: 11:39
 */
namespace giftbox\models;
class Coffret extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'coffret';
    protected $primaryKey = 'id';
    public $timestamps = false;

	public function coffretPresta() {
		return $this->hasMany('\giftbox\models\CoffretPresta', 'idcoffret');
	}
}