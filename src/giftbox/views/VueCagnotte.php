<?php
namespace giftbox\views;

use giftbox\models\Cagnotte;
use giftbox\models\Coffret;
use giftbox\models\Prestation;

class VueCagnotte
{
    public function render($url, $cagnotte, $liste, $cats, $app, $isAdmin=false, $giftLink='') {
		$cg = new templates\CardGestion();
		$btnClose = '';
		if ((isset($_SESSION['canAccessCoffretGestion'])) && ($cagnotte->cloturer == 0)) {
			$btnClose = <<<END
			
			<a href="{$app->urlFor('cagnotteClose', ['url'=>$url])}" class="ui button negative">Fermer la cagnotte</a>
END;
		}
		$listePresta = <<<END

		<section style="background-color:rgba(255, 255, 255, 0.25);display:block">
			<div class="ui indicating progress large yellow inverted" data-value="{$cagnotte->payer}" data-total="{$cagnotte->montant}">
				<div class="bar">
					<div class="progress" style="color:black"></div>
				</div>
				<div class="label" style="color:#ccc">{$cagnotte->payer}€ / {$cagnotte->montant}€</div>
			</div>
END;
		$listePresta .= '<div style="text-align:center">';
		if ($isAdmin && ($cagnotte->payer >= $cagnotte->montant)) {
			$listePresta .= <<<END

				<button class="ui button clipboard" data-clipboard-text="{$giftLink}" style="display:block;width:90%;max-width:400px;margin:0 auto;margin-bottom:2em;">Cliquez pour copier le lien d'accès au cadeau</button>
END;
		}
		foreach ($liste as $prests) {
			foreach ($prests->prestations as $p) {
				$listePresta .= $cg->render($p, $cats, $app);
			}
		}
		if ($cagnotte->payer < $cagnotte->montant) {
			$listePresta .= <<<END

					<br/><br/>
					<div class="ui two column stackable grid centered">
						<form class="ui form column" style="background-color:rgba(255, 255, 255, 0.25);border-radius:6px;" action="{$app->urlFor('cagnotteVisu', ['url'=>$url])}" method="POST">
							<div class="field">
							   <label for="name">Nom</label>
							   <input type="text" id="name" required placeholder="Votre Nom">
							</div>
							<div class="field">
							   <label for="montant">Montant</label>
							   <input type="text" id="montant" name="montant" required placeholder="Votre Montant">
							</div>
							<div class="field">
							   <label id="labmessage" for="message">Votre Message</label>
							   <textarea id="message" placeholder="Saisir votre message" required></textarea>
							</div>
							<div class="field">
							   <input type="submit" value="Participer" class="ui positive button small fluid">
							</div>
					   </form>
				   </div>
END;
		}
		$listePresta .= <<<END

				   <br/><br/>
				   {$btnClose}
			   </div>
		   </section>	
END;
		return $listePresta;
    }



    function renderNavbar() {
        global $MENU;
        $html = <<<END
<!-- Following Menu -->
		<div class="ui large top fixed hidden menu">
			<div class="ui container">
				<a class="item" href="{$MENU['home']}">Accueil</a>
				<a class="item" href="{$MENU['prestations']}">Prestations</a>
				<a class="item" href="{$MENU['categories']}">Catégories</a>
				<a class="item" href="{$MENU['coffret']}">Coffret</a>
			</div>
		</div>
		<!-- Navbar -->
		<div class="ui inverted vertical masthead center aligned segment">
			<div class="ui container">
				<div class="ui large secondary inverted pointing menu">
					<img src="%sgiftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
						<a class="ui inverted button" href="{$MENU['prestations']}">Prestations</a>
						<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
						<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
					</div>
				</div>
			</div>
		</div>
END;
        $html = sprintf($html, URL_IMAGES);
        return $html;
    }

}