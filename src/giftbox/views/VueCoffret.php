<?php
namespace giftbox\views;

use \giftbox\Panier;
use \giftbox\views\templates\CardGestion;

class VueCoffret
{
	function renderDiscoverAll($coffret, $liste, $cats, $app) {
		$prests = '';
		$cg = new CardGestion();
		foreach ($liste as $pr) {
			foreach ($pr->prestations as $p) {
				$prests .= '<div style="margin:0 10px;display:inline-block;">'.$cg->render($p, $cats, $app, true).'</div>';
			}
		}
		$html = <<<END

		<section>
			<div class="ui two column stackable grid centered" style="padding:2em;text-align:center;">
				<div class="column centered" style="text-align:center;background-color:#ddd;padding:3em;">
					<h1 class="ui header">Félicitations ! Vous avez reçu un cadeau !</h1>
					<h4><a href="mailto:{$coffret->email}">{$coffret->prenom} {$coffret->nom}</a> en est l'initiateur</h4>
					{$prests}
				</div>
			</div>
		</section>
END;
		return $html;
	}
	
	function renderDiscoverSingle($url, $coffret, $presta, $cats, $app, $nextId) {
		$cg = new CardGestion();
		$prests = $cg->render($presta, $cats, $app, true);
		$html = <<<END

		<section>
			<div class="ui two column stackable grid centered" style="padding:2em;text-align:center;">
				<div class="column centered" style="text-align:center;background-color:#ddd;padding:3em;">
					<h1 class="ui header">Félicitations ! Vous avez reçu un cadeau !</h1>
					<h3><a href="mailto:{$coffret->email}">{$coffret->prenom} {$coffret->nom}</a> en est l'initiateur</h3>
					<h5>Cliquez sur l'image pour passez à prestation suivante</h5>
					{$prests}
					<a href="{$app->urlFor('coffretDiscoverSingle', ['url'=>$url, 'pid'=>$nextId])}" class="ui button primary" style="display:block;margin-top:1em;">Cadeau suivant</a>
				</div>
			</div>
		</section>
END;
		return $html;
	}
	
	function renderPaymentClassic($app) {
		$html = <<<END

		<section>
			<form action="{$app->urlFor('coffretValidation')}" class="ui three column stackable grid centered form" method="POST" style="padding:2em;text-align:center;">
				<div class="column" style="background-color:#ddd;padding:3em;">
					<h4 class="ui dividing header">Informations de payement</h4>
					<div class="field">
						<label>Type de carte banquaire</label>
						<div class="ui selection dropdown">
							<input name="card[type]" type="hidden">
							<div class="default text">Type</div>
							<i class="dropdown icon"></i>
							<div class="menu">
								<div class="item" data-value="cb">
									<i class="credit card alternative icon"></i>CB
								</div>
								<div class="item" data-value="visa">
									<i class="visa icon"></i>Visa
								</div>
								<div class="item" data-value="mastercard">
									<i class="mastercard icon"></i>Mastercard
								</div>
								<div class="item" data-value="amex">
									<i class="amex icon"></i>American Express
								</div>
								<div class="item" data-value="discover">
									<i class="discover icon"></i>Discover
								</div>
							</div>
						</div>
					</div>
					<div class="fields">
						<div class="seven wide field">
      						<label>Numéro de carte</label>
      						<input name="card[number]" maxlength="16" placeholder="Numéro de carte" type="text">
						</div>
    					<div class="three wide field">
      						<label>CVC</label>
      						<input name="card[cvc]" maxlength="3" placeholder="Cryptogramme Visuel" type="text">
    					</div>
    					<div class="six wide field">
      						<label>Date d'expiration</label>
      						<div class="two fields">
        						<div class="field">
          							<select class="ui fluid search dropdown" name="card[expire-month]">
            							<option value="">Mois</option>
										<option value="1">Janvier</option>
										<option value="2">Février</option>
										<option value="3">Mars</option>
										<option value="4">Avril</option>
										<option value="5">Mai</option>
										<option value="6">Juin</option>
										<option value="7">Juillet</option>
										<option value="8">Août</option>
										<option value="9">Septembre</option>
										<option value="10">Octobre</option>
										<option value="11">Novembre</option>
										<option value="12">Décembre</option>
          							</select>
        						</div>
        						<div class="field">
          							<input name="card[expire-year]" maxlength="4" placeholder="Année" type="text">
        						</div>
      						</div>
    					</div>
  					</div>
					<input type="submit" name="submit-classic" class="ui button fluid" tabindex="0" value="Valider le payement"/>
				</div>
			</form>
		</section>
END;
		return $html;
	}
	
	function rendergestion($token,$app,$etat){
        
       
        $html = <<<END
					<!--<div class="ui action left icon input">
						<i class="search icon"></i>
						<input placeholder="Search..." type="text"/>
						<div class="ui teal button">Search</div>
					</div>
					<div class="ui right labeled left icon input">
						<i class="tags icon"></i>
						<input placeholder="Enter tags" type="text"/>
						<a class="ui tag label">Add tag</a>
					</div>-->
					
					
					

				<form action="{$app->urlFor('coffretGestion', ['token'=>$token])}" class="ui four column stackable grid centered form" method="POST" style="padding:2em;text-align:center;">
					<div class="column" style="background-color:#ddd;padding:3em;">
					
						 <div class="field">
							<label>Nouveaux Mot de Passe</label>
							<div class="field ui corner labeled left icon input">
								<i class="lock icon"></i>
				                <input placeholder="Nouveaux Mot de passe" name="passnew" type="password">
								<div class="ui corner label">
								</div>
							</div>
						</div>
                        
                        <div class="field">
							<label>Confirmer Mot de Passe</label>
							<div class="field ui corner labeled left icon input">
								<i class="lock icon"></i>
				                <input placeholder="Nouveaux Mot de passe" name="passnewcheck" type="password">
								<div class="ui corner label">
								</div>
							</div>
						</div>
                        
                        <div class="field">
							<label>Mot de Passe actuelle</label>
							<div class="field ui corner labeled left icon input">
								<i class="lock icon"></i>
				                <input placeholder="Mot de passe actuelle" name="passcheck" type="password">
								<div class="ui corner label">
								</div>
							</div>
						</div>
                        
							
						<div class="ui divider"></div>
						
						
                            	<input type="submit" name="submit-newpasswd" value="Modifier mot de passe" class="ui button positive"/>
                                <div class="ui divider"></div>
                                <label>Etat de votre coffret :</label>
                                <p>{$etat}</p>
						</div>
					</div>

                
				</form>
                
END;
        
		return $html;
	}
	
    
    function renderGestionSession($token,$app,$etat){
        
       
        $html = <<<END
					
					
					
	

				<form action="{$app->urlFor('coffretGestion', ['token'=>$token])}" class="ui four column stackable grid centered form" method="POST" style="padding:2em;text-align:center;">
					<div class="column" style="background-color:#ddd;padding:3em;">
					
                        
                        <div class="field">
							<label>Indiquer votre mot de passe</label>
							<div class="field ui corner labeled left icon input">
								<i class="lock icon"></i>
				                <input placeholder="Mot de passe" name="passAccess" type="password">
								<div class="ui corner label">
								</div>
							</div>
						</div>
                        
                      
                        
							
						<div class="ui divider"></div>
						
						
                            	<input type="submit" name="submit-askaccess" value="Verifier Mot de passe" class="ui button fluid positive"/>
						</div>
                        <div class="ui divider"></div>
					</div>
                    

          
				</form>
                
                
END;
        
		return $html;
	}
	
    
    
	function renderGetInfos($app) {
		$html = <<<END
					<!--<div class="ui action left icon input">
						<i class="search icon"></i>
						<input placeholder="Search..." type="text"/>
						<div class="ui teal button">Search</div>
					</div>
					<div class="ui right labeled left icon input">
						<i class="tags icon"></i>
						<input placeholder="Enter tags" type="text"/>
						<a class="ui tag label">Add tag</a>
					</div>-->
					
					
					
			
				<form action="{$app->urlFor('coffretRecap')}" class="ui four column stackable grid centered form" method="POST" style="padding:2em;text-align:center;">
					<div class="column" style="background-color:#ddd;padding:3em;">
					
						<div class="field">
							<label>Nom</label>
							<div class="field ui corner labeled left icon input">
								<i class="icon user"></i>
								<input placeholder="Votre nom" name="lastname" type="text" autofocus="on" required/>
								<div class="ui corner label">
									<i class="icon asterisk"></i>
								</div>
							</div>
						</div>
						<div class="field">
							<label>Prénom</label>
							<div class="field ui corner labeled left icon input">
								<i class="icon user"></i>
								<input placeholder="Votre prénom" name="firstname" type="text" required/>
								<div class="ui corner label">
									<i class="icon asterisk"></i>
								</div>
							</div>
						</div>
						<div class="field">
							<label>Email</label>
							<div class="field ui corner labeled left icon input">
								<i class="icon mail"></i>
								<input placeholder="Votre email" name="email" type="email" required/>
								<div class="ui corner label">
									<i class="icon asterisk"></i>
								</div>
							</div>
						</div>
                        
                        <div class="field">
							<label>Mot de Passe</label>
							<div class="field ui corner labeled left icon input">
								<i class="lock icon"></i>
				                <input placeholder="Mot de passe (non obligatoire)" name="pass" type="password">
								<div class="ui corner label">
								</div>
							</div>
						</div>
							
						<div class="ui divider"></div>
						
						<div class="grouped fields">
							<label for="fruit">Selectionnez votre mode de paiement :</label>
							<div class="field">
								<div class="ui radio checkbox">
									<input name="paiement" checked="" tabindex="0" value="classic" class="hidden" type="radio">
									<label>Classique</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input name="paiement" tabindex="0" value="cagnotte" class="hidden" type="radio">
									<label>Cagnotte</label>
								</div>
							</div>
						</div>
					</div>
                    
                    
					<div class="ui column" style="background-color:#ddd;padding:2em;border-left:1px solid rgba(34,36,38,.15);">
						<div class="field">
							<label for="input-message">Message</label>
							<textarea id="input-message" name="message" placeholder="Votre message" required></textarea>
						</div>
						<div class="ui divider"></div>
						<input type="submit" name="submit" value="Procéder au payement" class="ui button positive"/>
					</div>
                    
    
				</form>
                
END;
		return $html;
	}
	
	
	function renderNavbar() {
		global $MENU;
		$html = <<<END
<!-- Following Menu -->
		<div class="ui large top fixed hidden menu">
			<div class="ui container">
				<a class="item" href="{$MENU['home']}">Accueil</a>
				<a class="item" href="{$MENU['prestations']}">Prestations</a>
				<a class="item" href="{$MENU['categories']}">Catégories</a>
				<a class="item active">Coffret</a>
			</div>
		</div>
		<!-- Navbar -->
		<div class="ui inverted vertical masthead center aligned segment">
			<div class="ui container">
				<div class="ui large secondary inverted pointing menu">
					<img src="%sgiftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
						<a class="ui inverted button" href="{$MENU['prestations']}">Prestations</a>
						<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
						<a class="ui inverted button active">Coffret</a>
					</div>
				</div>
			</div>
		</div>
END;
		$html = sprintf($html, URL_IMAGES);
		return $html;
	}
}