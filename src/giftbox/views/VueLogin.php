<?php
namespace giftbox\views;

class VueLogin
{
	public function render($app) {
		$loginTemplate = new templates\LoginTemplate();
		$navbar = $this->renderNavbar($app);
		$html = <<<END

	<!-- Page Contents -->
	<div class="content">
		$navbar
        {$loginTemplate->render($app)}
	</div>
END;
		return $html;
	}

	function renderNavbar($app) {
		global $MENU;
		$html = <<<END
<!-- Following Menu -->
		<div class="ui large top fixed hidden menu">
			<div class="ui container">
				<a class="item" href="{$MENU['home']}">Accueil</a>
				<a class="item" href="{$MENU['prestations']}">Prestations</a>
				<a class="item" href="{$MENU['categories']}">Categories</a>
				<a class="item" href="{$MENU['coffret']}">Coffret</a>
			</div>
		</div>
		<!-- Navbar -->
		<div class="ui inverted vertical masthead center aligned segment">
			<div class="ui container">
				<div class="ui large secondary inverted pointing menu">
					<img src="%sgiftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
						<a class="ui inverted button" href="{$MENU['prestations']}">Prestations</a>
						<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
						<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
					</div>
				</div>
			</div>
		</div>
END;
		$html = sprintf($html, URL_IMAGES);
		return $html;
	}
}