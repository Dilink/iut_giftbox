<?php
namespace giftbox\views;

class VueCatalogue
{
	private $isLogged;
	
	function __construct($connected) {
		$this->isLogged = $connected;
	}
	
	public function render($liste, $sel, $app, $catsList=null) {
		$navbar = $this->renderNavbar($sel === 1);
		$content = '';
		if ($sel == 1) {
			$cardTemplate = new templates\CardCategorieThumbTemplate();
			$content .= '<section class="ui centered special link four cards" style="padding:3em;">';
			foreach ($liste as $c) {
				$content .= $cardTemplate->render($c, $app);
			}
			$content .= "\n\t\t</section>";
		} else if ($sel == 2) {
			if (count($liste) > 0) {
				$cardTemplate = new templates\CardPrestationThumbTemplate($this->isLogged);
				$content .= '<section class="ui centered special link six cards">';
				foreach ($liste as $c) {
					$content .= $cardTemplate->render($c, $app, $catsList);
				}
				$content .= "\n\t\t</section>";
			} else {
				$content .= '<section class="ui centered" style="text-align:center;">';
				$content .= '<a href="'.$app->urlFor('categories').'" class="ui button big">Catégorie inconnue ou aucune prestation n\'est liée à cette catégorie</a>';
				$content .= "\n\t\t</section>";
			}
		}
		$html = <<<END

	<!-- Page Contents -->
	<div class="content">
		$navbar
		$content
	</div>
END;
		return $html;
	}
	
	function renderNavbar($areMultiple=true) {
		global $MENU;
		$caLink = $areMultiple ? 'active' : '" href="'.$MENU['categories'];
		$html = <<<END
<!-- Following Menu -->
		<div class="ui large top fixed hidden menu">
			<div class="ui container">
				<a class="item" href="{$MENU['home']}">Accueil</a>
				<a class="item" href="{$MENU['prestations']}">Prestations</a>
				<a class="item {$caLink}">Catégories</a>
				<a class="item" href="{$MENU['coffret']}">Coffret</a>
			</div>
		</div>
		<!-- Navbar -->
		<div class="ui inverted vertical masthead center aligned segment">
			<div class="ui container">
				<div class="ui large secondary inverted pointing menu">
					<img src="%sgiftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
						<a class="ui inverted button" href="{$MENU['prestations']}">Prestations</a>
						<a class="ui inverted button {$caLink}">Catégories</a>
						<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
					</div>
				</div>
			</div>
		</div>
END;
		$html = sprintf($html, URL_IMAGES);
		return $html;
	}
}