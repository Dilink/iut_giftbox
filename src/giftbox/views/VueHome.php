<?php
namespace giftbox\views;

use \giftbox\models\Prestation;
use \giftbox\models\Categorie;

class VueHome
{
	private $isLogged;
	
	function __construct($connected) {
		$this->isLogged = $connected;
	}
	
	public function render($app, $listeBestPrestas, $listeCats) {
		global $MENU;
		$cardTemplate = new templates\CardPrestationThumbTemplate($this->isLogged);
		$bestPrestas = '<section class="ui centered special link four cards">';
		foreach ($listeBestPrestas as $p) {
			$bestPrestas .= $cardTemplate->render($p, $app, $listeCats);
		}
		$bestPrestas .= "\n\t\t</section>";

		$html = <<<END
		<!-- Following Menu -->
		<!--<div class="ui inverted large top fixed hidden menuc menu">
			<div class="ui container">
				<div class="ui large secondary pointing menu">
					<img src="giftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						<a class="ui inverted button active">Accueil</a>
						<a class="ui inverted button" href="{$MENU['prestations']}">Prestations</a>
						<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
						<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
						<!--
						<a class="item active">Accueil</a>
						<a class="item" href="{$MENU['prestations']}">Prestations</a>
						<a class="item" href="{$MENU['categories']}">Catégories</a>
						<a class="item" href="{$MENU['coffret']}">Coffret</a>
						--
					</div>
				</div>
			</div>
		</div>-->

		<!-- GOOD
		<div class="ui inverted large top fixed hidden menu menuc">
		<div class="ui inverted /*vertical masthead center aligned segment*/ top fixed menuc hidden">
			<div class="ui container">
				<!--<div class="ui large secondary inverted pointing menu">
					<img src="giftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
						<a class="ui inverted button active">Prestations</a>
						<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
						<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
					</div>
				<!--</div>
			</div>
		</div>-->


		<!-- CUSTOM
		<div class="following-menu">
			<div class="container">
				<img src="giftbox.png" class="ui tiny image">
				<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
				<div class="right item">
					<a class="ui button" href="{$MENU['home']}">Accueil</a>
					<a class="ui button active">Prestations</a>
					<a class="ui button" href="{$MENU['categories']}">Catégories</a>
					<a class="ui button" href="{$MENU['coffret']}">Coffret</a>
				</div>
			</div>
		</div>-->




		<!--<div class="ui inverted large top fixed hidden menu menuc">
			<div class="ui container">
				<img src="%sgiftbox.png" class="ui tiny image">
				<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
				<div class="right item">
					<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
					<a class="ui inverted button active">Prestations</a>
					<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
					<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
				</div>
			</div>
		</div>-->




		<!-- Page Contents -->
		<div class="pusher">
			<div class="ui inverted vertical masthead masthead-image center aligned segment">
				<div class="ui container">
					<div class="ui large secondary inverted pointing menu">
						<img src="%sgiftbox.png" class="ui tiny image">
						<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
						<div class="right item">
							<a class="ui inverted button active">Accueil</a>
							<a class="ui inverted button" href="{$MENU['prestations']}">Prestations</a>
							<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
							<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
						</div>
					</div>
				</div>
				<div class="ui text container">
					<h1 class="ui inverted header">GiftBox</h1>
					<div class="ui shape text">
						<div class="sides">
							<h2 class="ui header side active">Offrez de magnifiques cadeaux à vos proches !</h2>
							<h2 class="ui header side">Un cadeau magique pour un proche magique !</h2>
							<h2 class="ui header side">Un cadeau trouvé, le plaisir à portée.</h2>
                            <h2 class="ui header side">Un cadeau acheté, un heureux offert !</h2>
                            <h2 class="ui header side">Qui à dit que le père noel n'existait pas ? C'est vous !</h2>
                            <h2 class="ui header side">Prennez part à des expériences hors du commun !</h2>
                            <h2 class="ui header side"></h2>
                            
						</div>
					</div>
					<a class="ui huge primary button" href="{$MENU['categories']}">C'est parti !</a>
				</div>
			</div>
			<div class="ui vertical stripe segment">
				<div class="ui middle aligned stackable grid container">
					<div class="row">
						<div class="column">
							<h3 class="ui header centered" style="color: #cccccc;"><u>Nos meilleures prestations</u></h3>
							$bestPrestas
						</div>
					</div>
				</div>
			</div>
		</div>
END;
		$html = sprintf($html, URL_IMAGES, URL_IMAGES);
		return $html;
	}
}