<?php
namespace giftbox\views\templates;

class CardCategorieThumbTemplate
{
	public function render($cat, $app) {
	    $catImg = $cat->img;

		$html = <<<END
		
			<div class="ui card">
				<div class="content">
					<div class="header">{$cat->nom}</div>
					<div class="ui divider"></div>
					<img class="ui wireframe image" src="%sthumbs/{$catImg}">
					<img class="ui wireframe image" src="%swireframe_paragraph.png">
				</div>
				<div class="extra content">
					<a class="ui button" href="{$app->urlFor('categorie', array('id'=>$cat->id))}">Découvrir les prestations</a>
				</div>
			</div>
END;
		$html = sprintf($html, URL_IMAGES, URL_IMAGES);
		return $html;
	}
}