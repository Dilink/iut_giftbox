<!DOCTYPE html>
<html lang="fr" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="shortcut icon" href="<?php echo URL_IMAGES;?>favicon.ico" type="images/x-icon">
    <title>Giftbox</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS;?>semantic.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS;?>custom.css">
<!--    <link href="<?php echo URL_CSS;?>styles.css" rel="stylesheet">-->
	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
