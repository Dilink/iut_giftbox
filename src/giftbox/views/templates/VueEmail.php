<?php
namespace giftbox\views\templates;

class VueEmail
{
	public function render($urlGestionCoffret, $app, $noncagnotte=null) {
        $urlcagnotte='';
        if($noncagnotte!=null){
            $urlcagnotte = <<<END

                            <br/>
							<h2>Vous pouvez cliquer <a href="{$app->urlFor('cagnotteVisu', ['url'=>$noncagnotte])}">ici pour la participation</a> à la cagnotte.</h2>
							<br/>
END;
        }
		$html = <<<END

				<div class="ui middle aligned center aligned grid" style="padding:6em 0;background-color:#ccc;width:50%%; margin:0 auto;">
					<div class="column">
						<div class="ui large">
							<h2>Vous pouvez cliquer <a href="{$app->urlFor('coffretGestion', ['token'=>$urlGestionCoffret])}">ici pour la gestion</a> du coffret.</h2>
							<br/>{$urlcagnotte}    
						</div>
					</div>
				</div>
END;
		return $html;
	}
    
  public function renderCagnotte($app,$noncagnotte=null){
       $urlcagnotte='';
        if($noncagnotte!=null){
            $urlcagnotte = <<<END
                        <div class="ui middle aligned center aligned grid" style="padding:0.5em 0;background-color:#ccc;width:10%%; margin:0 auto;">
					<div class="column">
						<div class="ui large">
                            <br/>
							<h2>Vous pouvez cliquer <a href="{$app->urlFor('cagnotteVisu', ['url'=>$noncagnotte])}">ici pour la participation</a> à la cagnotte.</h2>
							<br/>
                            </div>
					</div>
				</div>
END;
        }
      return $urlcagnotte;
  }
}