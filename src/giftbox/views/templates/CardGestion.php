<?php
namespace giftbox\views\templates;

class CardGestion
{
	public function render($presta, $cats, $app, $isGift=false) {
		$priceField = $category = '';
		if (!$isGift) {
			$catname = $cats[$presta->cat_id];
			$priceField = <<<END

							<div class="ccard__date">
								<span class="ccard__date__month">{$presta->prix}€</span>
							</div>
END;
			$category = <<<END

								<div class="ccard__category">{$catname}</div>
END;
		}
		$html = <<<END
		
						<article class="ccard">
							<header class="ccard__thumb">
								<img src="%sthumbs/{$presta->img}" alt="Image de: {$presta->nom}">
							</header>{$priceField}
							<div class="ccard__body">{$category}
								<h2 class="card__title">{$presta->nom}</h2>
								<p class="ccard__description">
									{$presta->descr}
								</p>
							</div>
						</article>
END;
		$html = sprintf($html, URL_IMAGES);
		return $html;
	}
}