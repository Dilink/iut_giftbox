<?php
namespace giftbox\views\templates;

use \giftbox\Panier;
use \giftbox\models\PrestationCoffret;

class ShoppingCartTemplate
{
	private $isRenderLinkNext = true;
	
	public function renderLinkNext($bool) {
		$this->isRenderLinkNext = $bool;
	}
	
    public function rendergestion($items, $isAcceptable, $app, $urlPost='coffretInfos', $urlName='Poursuivre la commande') {
		$prixTotal = 0;
		$tbodyContent = '';
		$disabledClass = $isAcceptable ? 'positive' : ' disabled';
		$disabledText = $isAcceptable ? $urlName : 'Veuillez choisir au moins 2 prestations, de 2 catégories différentes.';
		if (count($items) > 0) {
			foreach($items as $it) {
            
				$itcount = Panier::$saved_cart[$it->id]['q'];
				$ittot = number_format($it->prix * $itcount, 2);
				$prixTotal += $ittot;
				$tbodyContent .= <<<TBODY

						<tr>
							<td class="left aligned"><a target="_blank" href="{$app->urlFor('prestation', ['id'=>$it->id])}" class="external-link">{$it->nom} <i class="external url icon"></i></a></td>
							<td>{$it->prix}€</td>
							<td>{$itcount}</td>
							<td>{$ittot}€</td>
							
						</tr>
TBODY;
			}
		} else {
			$tbodyContent .= <<<EMPTY

						<tr style="background-color:#eee;height:5em;">
							<td colspan="5" class="center aligned">Le coffret est vide.</td>
						</tr>
EMPTY;
		}
		$prixTotal = number_format($prixTotal, 2);
		$tbodyContent .= <<<TEND

						<tr style="background-color:#eee;">
							<td>Prix total :</td>
							<td colspan="3">{$prixTotal}€</td>
							<td></td>
						</tr>
TEND;
		
		$linkNext = '';
		if ($this->isRenderLinkNext) {
			$linkNext = <<<END
			
						<tr>
							<td colspan="4"><a href="{$app->urlFor($urlPost)}" style="border-right:none;" class="ui button {$disabledClass}">{$disabledText}</a></td>
							<td style="background-color:rgba(0,0,0,.03);border-left:none;"></td>
						</tr>		
END;
		}
		
		
		$html = <<<END

		<h1 style="color:#ccc;text-align:center;"><u>Coffret :</u></h1><br/>
		<div class="ui two column relaxed grid centered">
			<div class="column">
				<table class="ui definition celled right aligned table">
					<thead>
						<tr>
							<th></th>
							<th>Prix unitaire</th>
							<th>Quantité</th>
							<th>Prix du lot</th>
							<th></th>
						</tr>
					</thead>
					<tbody>{$tbodyContent}{$linkNext}
					</tbody>
				</table>
			</div>
		</div>
		<br/>
END;
		return $html;
	}	


	public function render($items, $isAcceptable, $app, $urlPost='coffretInfos', $urlName='Poursuivre la commande') {
		$prixTotal = 0;
		$tbodyContent = '';
		$disabledClass = $isAcceptable ? 'positive' : ' disabled';
		$disabledText = $isAcceptable ? $urlName : 'Veuillez choisir au moins 2 prestations, de 2 catégories différentes.';
		if (count($items) > 0) {
			foreach($items as $it) {
				$itcount = Panier::$saved_cart[$it->id]['q'];
				$ittot = number_format($it->prix * $itcount, 2);
				$prixTotal += $ittot;
				$tbodyContent .= <<<TBODY

						<tr>
							<td class="left aligned"><a target="_blank" href="{$app->urlFor('prestation', ['id'=>$it->id])}" class="external-link">{$it->nom} <i class="external url icon"></i></a></td>
							<td>{$it->prix}€</td>
							<td>{$itcount}</td>
							<td>{$ittot}€</td>
							<td><a href="{$app->urlFor('coffretDelete', ['id'=>$it->id])}" class="ui icon button negative"><i class="trash icon"></i></a></td>
						</tr>
TBODY;
			}
		} else {
			$tbodyContent .= <<<EMPTY

						<tr style="background-color:#eee;height:5em;">
							<td colspan="5" class="center aligned">Le coffret est vide.</td>
						</tr>
EMPTY;
		}
		$prixTotal = number_format($prixTotal, 2);
		$tbodyContent .= <<<TEND

						<tr style="background-color:#eee;">
							<td>Prix total :</td>
							<td colspan="3">{$prixTotal}€</td>
							<td></td>
						</tr>
TEND;
		
		$linkNext = '';
		if ($this->isRenderLinkNext) {
			$linkNext = <<<END
			
						<tr>
							<td colspan="4"><a href="{$app->urlFor($urlPost)}" stye="border-right:none;" class="ui button {$disabledClass}">{$disabledText}</a></td>
							<td style="background-color:rgba(0,0,0,.03);border-left:none;"></td>
						</tr>		
END;
		}
		
		
		$html = <<<END

		<h1 style="color:#ccc;text-align:center;"><u>Coffret :</u></h1><br/>
		<div class="ui two column relaxed grid centered">
			<div class="column">
				<table class="ui definition celled right aligned table">
					<thead>
						<tr>
							<th></th>
							<th>Prix unitaire</th>
							<th>Quantité</th>
							<th>Prix du lot</th>
							<th></th>
						</tr>
					</thead>
					<tbody>{$tbodyContent}{$linkNext}
					</tbody>
				</table>
			</div>
		</div>
		<br/>
END;
		return $html;
	}	
}
	