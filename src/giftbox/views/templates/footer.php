    <div class="ui inline fixed bottom cookie nag">
		<span class="title">We use cookies to ensure you get the best experience on our website</span>
		<i class="close icon"></i>
	</div>
	<div class="ui inverted vertical footer segment" style="padding-bottom:40px;">
		<div class="ui center aligned container">
			<p style="color:#494949">L’abus d’alcool est dangereux pour la santé, à consommer avec modération.</p>
			<div class="ui inverted section divider"></div>
			<p style="color:rgba(255,255,255,.5)">Copyright HHL 2017</p>
			<img src="<?php echo URL_IMAGES;?>giftbox.png" class="ui centered tiny image">
			<p style="color:rgba(255,255,255,.5);margin-top:10px">Site réalisé par le groupe HHL &copy; : { Hervieux, Holzer, Laghouati }</p>
			<div class="ui horizontal inverted small divided link list">
				<a class="item" href="#">Site Map</a>
				<a class="item" href="#">Contact Us</a>
				<a class="item" href="#">Terms and Conditions</a>
				<a class="item" href="#">Privacy Policy</a>
				<?php if (!isset($_SESSION['id'])): ?>
                    <a class="item" href="<?php echo $app->urlFor('login'); ?>">Se connecter</a>
				<?php else: ?>
                    <a class="item" href="<?php echo $app->urlFor('logout'); ?>">Se déconnecter</a>
				<?php endif ?>
			</div>
		</div>
	</div>
	<!-- JS -->
	<script src="<?php echo URL_JS;?>jquery-3.1.1.min.js"></script>
	<script src="<?php echo URL_JS;?>jquery.cookie.min.js"></script>
	<script src="<?php echo URL_JS;?>semantic.min.js"></script>
	<script src="<?php echo URL_JS;?>clipboard.min.js"></script>
	<script src="<?php echo URL_JS;?>custom.js"></script>
</body>
</html>