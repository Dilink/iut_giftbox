<?php
namespace giftbox\views\templates;

class LoginTemplate
{
	public function render($app) {
		$html = <<<END

        <div class="ui middle aligned center aligned grid" style="width:50%%; margin:0 auto;">
            <div class="column">
                <h2 class="ui teal image header">
                    <img src="%sgiftbox.png" class="image">
                    <div class="content">Log-in to your account</div>
                </h2>
                <form class="ui large form" method="POST" action="{$app->urlFor('login')}">
                    <div class="ui stacked segment">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="user icon"></i>
                                <input type="text" name="email" placeholder="E-mail address">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="ui fluid large teal submit button">Login</div>
                    </div>
                    <div class="ui error message"></div>
                </form>
            </div>
        </div>
END;
		$html = sprintf($html, URL_IMAGES);
        return $html;
	}
}