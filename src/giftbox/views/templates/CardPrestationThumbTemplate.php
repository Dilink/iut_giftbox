<?php
namespace giftbox\views\templates;

use \giftbox\Panier;

class CardPrestationThumbTemplate
{
	private $isLogged;
	
	function __construct($connected) {
		$this->isLogged = $connected;
	}
	
	public function render($presta, $app, $catsList) {
		$description = $presta['descr'];
		$descriptionLength = 100;
		$dots = '...';
		$descr = (strlen($description) > $descriptionLength) ? substr($description, 0, $descriptionLength - strlen($dots)) . $dots : $description;

		if ($catsList == null) {
			$cat = $presta['categorie']['nom'];
		} else {
			$cat = $catsList[$presta['cat_id']];
		}
		
		if (Panier::hasItem($presta['id'])) {
			$btnAddToCart = <<<END
<div>Déjà ajoutée</div>
END;
		} else {
			$btnAddToCart = <<<END
<a href="{$app->urlFor('coffretAdd', ['id'=>$presta['id']])}" class="ui primary button mini fluid">Ajouter au panier</a>
END;
		}
		
		$btnAdmin = '';
		if ($this->isLogged) {
			$isVisible = 0;
			if ($presta['isVisible'] === 0) {
				$isVisible = 1;
			}
			$btnAdmin .= "<a href=\"{$app->urlFor('prestationVisibility', ['id'=>$presta['id'], 'visible'=>$isVisible])}\" class=\"ui icon button circular mini\">";
			if ($presta['isVisible'] === 1) {
				$btnAdmin .= "\n\t\t\t\t\t\t".'<i class="unhide icon" style="font-size: 1.5em;"></i>';
			} else {
				$btnAdmin .= "\n\t\t\t\t\t\t".'<i class="hide icon" style="font-size: 1.5em;"></i>';
			}
			$btnAdmin .= <<<END

					</a>
					<a href="{$app->urlFor('prestationDelete', ['id'=>$presta['id']])}" class="ui icon button circular mini negative">
						<i class="trash icon" style="font-size: 1.5em;"></i>
					</a>
					<div class="ui divider"></div>
END;
		}
		
		$html = <<<END
			<div class="card">
				<!--<div class="attached-label" style="border-top-right-radius: 0 !important;border-bottom-right-radius: 4px !important;">{$cat}</div>-->
				<!--<div class="ui black ribbon label">
					<i class="hotel icon"></i> Hotel
				</div>-->
				<div class="blurring dimmable image">
					<!--<a class="ui red ribbon label">Overview</a>-->
					<div class="ui dimmer transition hidden">
						<div class="content">
							<div class="center">
								<a href="{$app->urlFor('prestation', array('id'=>$presta['id']))}" class="ui inverted button">Voir le detail</a>
							</div>
						</div>
					</div>
					<img src="%sthumbs/{$presta['img']}" alt="{$presta['img']}">
				</div>
				<div class="content">
					<div class="header">{$presta['nom']}</div>
					<div class="meta">
						<div class="group">{$cat}</div>
					</div>
					<div class="description">{$descr}</div>
				</div>
				<div class="extra content">
					{$btnAdmin}
					{$btnAddToCart}
					<div class="ui divider"></div>
					<div class="right floated created">{$presta['prix']}€</div>
					<div class="extra aligned">
						<div data-rating="{$presta['note']}" class="ui star rating"></div>
					</div>
                </div>
        </div>
END;
		$html = sprintf($html, URL_IMAGES);
		return $html;
	}
}