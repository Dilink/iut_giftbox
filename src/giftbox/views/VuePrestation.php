<?php
namespace giftbox\views;

use \giftbox\models\Prestation;

class VuePrestation
{
	private $isLogged;
	
    public $prestAlea = null;
	public $shoppingCart = null;
	public $prestaUserNote = 0;
	
	function __construct($connected) {
		$this->isLogged = $connected;
	}
	
	public function render($liste, $sel, $app, $catsList=null, $tri=-1) {
		$cardTemplate = new templates\CardPrestationThumbTemplate($this->isLogged);
		$navbar = $this->renderNavbar($app, $tri, $sel === 1);
		$content = '';
		$notationURL = '';
		if ($sel == 1) {
			$content .= '<section class="ui centered special link  cards">';
			if ($this->isLogged) {
				$categorieInputs = '';
				foreach ($catsList as $ckey => $cval) {
					$categorieInputs .= <<<END

							<div class="item" data-value="$ckey">$cval</div>
END;
				}
				$content .= <<<END

			<form method="POST" action="{$app->urlFor('prestationAdd')}" class="card ui form" enctype="multipart/form-data">
				<div class="content">
					<div class="header" style="text-align:center;">Ajouter une prestation</div>
				</div>
				<div class="content">
					<div class="field">
						<img id="image-upload-preview" src="#" alt="Your image" />
						<input type="file" name="pictures" id="fileupload" class="inputfile" required />
						<label for="fileupload">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg>
							<span>Choisissez une image...</span>
						</label>
					</div>
					<div class="field">
						<label>Titre</label>
						<input type="text" name="title" placeholder="Titre" required />
					</div>
					<div class="field">
						<label>Description</label>
						<textarea placeholder="Description" name="description" required></textarea>
					</div>
					<div class="field ui selection dropdown fluid">
						<input name="category" type="hidden" name="category" required>
						<div class="default text">Catégorie</div>
						<i class="dropdown icon"></i>
						<div class="menu">{$categorieInputs}
						</div>
					</div>
					<div class="field">
						<div class="ui right labeled input fluid">
							<div class="ui label">Prix</div>
							<input placeholder="Montant" type="text" name="price" required>
							<div class="ui basic label">€</div>
						</div>
					</div>
				</div>
				<div class="extra content">
					<input type="submit" name="submit" class="ui positive button mini fluid" value="Créer la prestation" />
				</div>
			</form>
			<script>
				(function() {
					var input = document.querySelector('#fileupload');
					input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
					input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
				})();
			</script>
END;
			}
			foreach ($liste as $p) {
				$content .= $cardTemplate->render($p, $app, $catsList);
			}
			$content .= "\n\t\t</section>";
		} else if ($sel == 2) {
			if ($liste !== null) {
				$content .= $this->getDetailHTML($liste, $app, $catsList);
				if (($sel == 2) && ($this->prestaUserNote === 0)) {
					$notationURL = <<<JS
					<script>
						var notationURL = window.location.origin + '{$app->urlFor('prestationNotation', ['id'=>$liste->id])}';
					</script>
JS;
				}
			} else {
				$content .= '<section class="ui centered" style="text-align:center;">';
				$content .= '<a href="'.$app->urlFor('prestations').'" class="ui button big">Prestation inconnue.</a>';
				$content .= "\n\t\t</section>";
			}
		}
		
		$cartItems = '';
		$cartItemsTotal = 0;
		$cartItemsCount = count($this->shoppingCart);
		if ($cartItemsCount > 0) {
			foreach ($this->shoppingCart as $ci) {
				$cartItems .= "<div><span><a href=\"{$app->urlFor('coffretDelete', ['id'=>$ci->id])}\" class=\"delete-icon\">&times;</a><a href=\"{$app->urlFor('prestation', ['id'=>$ci->id])}\" style=\"color:#ccc;\">{$ci->nom}</a></span><span>{$ci->prix}€</span></div>";
				$cartItemsTotal += $ci->prix;
			}
		} else {
			$cartItems = '<div>Aucune prestation ajoutée.</div>';
		}
		$cartItemsTotal = number_format($cartItemsTotal, 2);
		$html = <<<END

	<!-- Page Contents -->
	<div class="content">
		$navbar
		
		<div class="shopping-cart">
			<div class="shopping-cart-btn"></div>
			<div class="shopping-cart-content">
				<h2>Panier</h2>
				<hr>
				{$cartItems}
				<hr>
				<h3>Total</h3>
				<div><span>{$cartItemsCount} articles</span><span>{$cartItemsTotal}€</span></div><br/>
				<a href="{$app->urlFor('coffretVisu')}" class="ui button green tiny">Voir le coffret</a>
			</div>
		</div>
		
		$content
		$notationURL
	</div>
END;
		return $html;
	}

	function getDetailHTML($presta, $app, $catsList) {
        $content = ' ';
        $cardTemplate = new templates\CardPrestationThumbTemplate($this->isLogged);
        $content .= '<section class="ui centered special link two cards">';
        foreach ($this->prestAlea as $c) {
            $content .= $cardTemplate->render($c, $app, $catsList);
        }
        $content .= "\n\t\t</section>";

        $imgPresta = $presta->img;
        $descrPresta = $presta->descr;


        /* <h1>{$presta->nom}</h1>
        <img src="%s/thumbs/{$imgPresta}" alt=""> <br> <br> <br>
        <p> $descrPresta </p>
        $html = <<<END
	   <div class="Presta">
            <div class="content">
                         <br> <br>
                         <div class="nomPresta"><h1>{$presta->nom}</h1></div>
                         <br> <br>
                         <div class="blurring dimmable image">
                             <img class="imgPresta" src="%sthumbs/{$imgPresta}" alt="{$imgPresta}">
                             <br> <br> 
                         </div>
                        <div class="descriptionPresta">
                            <p> Description : </p>
                            <p> $descrPresta </p> 
                            <br> <br> 
                         </div>
            </div>
            <div class="extra content">
                    <div class="prixPresta">
                        <a class="right floated created">{$presta['prix']}€</a>
                        <br> <br>
                    </div>
                    <div class="extra aligned">
                        <div class="notePresta">
                            <div data-rating="{$presta['note']}" class="ui star rating"></div>
                            <br> <br>
                        </div>
                    </div>
            </div>
        </div>
END;
        */

        $html = <<<END
        <section class="Presta">
			<div>
				<div class="gauchePresta">

					 <br> <br>
						  <div class="nomPresta"><h1>{$presta->nom}</h1></div>
						  <br> <br>
						  <div class="blurring dimmable image">
							 <img class="imgPresta" src="%sthumbs/{$imgPresta}" alt="{$imgPresta}">
							 <br> <br> 
						  </div>    
				</div>
				<div class="droitePresta">
					<div class="descriptionPresta">
								<br><br>
								<p>Description :</p>
								<p>$descrPresta</p>
								<br><br>
					</div>
					<div class="extra content">
						<div class="prixPresta">
							<div class="ui statistic">
								<div class="label">Prix</div>
								<div class="value">{$presta['prix']}€</div>
							</div>
						</div>
						<div class="extra aligned">
							<div class="notePresta">
								<br/>
								<div data-rating="{$this->prestaUserNote}" class="ui star rating editable massive"></div>
								<br><br>
							</div>
						</div>
						<div class="PrestaAlea">
							<h3>Nos clients aiment aussi :</h3>
							$content
						</div>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
        </section>
    
END;

        $html = sprintf($html, URL_IMAGES);
        return $html;
	}

	function renderNavbar($app, $triId, $areMultiple=true) {
		global $MENU;
		$prLink = $areMultiple ? 'active' : '" href="'.$MENU['prestations'];
		$tri = '';
		$triTypes = array('Prix croissant', 'Prix décroissant', 'Ordre alphabétique', 'Appréciations', 'Catégorie', 'Prix entre 0€ et 10€', 'Prix entre 10€ et 25€', 'Prix entre 25€ et 50€', 'Prix entre 50€ et 100€', 'Prix entre 100€ et 150€', 'Prix entre 150€ et 200€', 'Prix entre 200€ et 250€', 'Prix entre 250€ et 300€', 'Prix supérieur à 300€');
		$triTypeStr = '...';
		if ($triId > -1) {
			$triTypeStr = ': '.$triTypes[$triId];
		}
		if ($areMultiple) {
			$tri = <<<DROPDOWN
							<div class="ui selection dropdown">
								<div class="default text">Trier par{$triTypeStr}</div>
								<i class="dropdown icon"></i>
								<div class="menu">
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>0))}" data-value="0">{$triTypes[0]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>1))}" data-value="1">{$triTypes[1]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>2))}" data-value="2">{$triTypes[2]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>3))}" data-value="3">{$triTypes[3]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>4))}" data-value="4">{$triTypes[4]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>5, 'prixmin'=>0, 'prixmax'=>10))}">{$triTypes[5]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>6, 'prixmin'=>10, 'prixmax'=>25))}">{$triTypes[6]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>7, 'prixmin'=>25, 'prixmax'=>50))}">{$triTypes[7]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>8, 'prixmin'=>50, 'prixmax'=>100))}">{$triTypes[8]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>9, 'prixmin'=>100, 'prixmax'=>150))}">{$triTypes[9]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>10, 'prixmin'=>150, 'prixmax'=>200))}">{$triTypes[10]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>11, 'prixmin'=>200, 'prixmax'=>250))}">{$triTypes[11]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>12, 'prixmin'=>250, 'prixmax'=>300))}">{$triTypes[12]}</a>
									<a class="item" href="{$app->urlFor('prestationsTri', array('tri'=>13, 'prixmin'=>300, 'prixmax'=>0))}">{$triTypes[13]}</a>
								</div>
							</div>	
DROPDOWN;
		}
		$html = <<<END
<!-- Following Menu -->
		<div class="ui large top fixed hidden menu">
			<div class="ui container">
				<a class="item" href="{$MENU['home']}">Accueil</a>
				<a class="item {$prLink}">Prestations</a>
				<a class="item" href="{$MENU['categories']}">Categories</a>
				<a class="item" href="{$MENU['coffret']}">Coffret</a>
			</div>
		</div>
		<!-- Navbar -->
		<div class="ui inverted vertical masthead center aligned segment">
			<div class="ui container">
				<div class="ui large secondary inverted pointing menu">
					<img src="%sgiftbox.png" class="ui tiny image">
					<h1 style="padding-left: 12px;margin-top: auto;">GIFTBOX</h1>
					<div class="right item">
						{$tri}
						<a class="ui inverted button" href="{$MENU['home']}">Accueil</a>
						<a class="ui inverted button {$prLink}">Prestations</a>
						<a class="ui inverted button" href="{$MENU['categories']}">Catégories</a>
						<a class="ui inverted button" href="{$MENU['coffret']}">Coffret</a>
					</div>
				</div>
			</div>
		</div>
END;
		$html = sprintf($html, URL_IMAGES);
		return $html;
	}
}