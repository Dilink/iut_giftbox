<?php
namespace giftbox\controllers;

class DebugController extends AbstractController
{
	public function generateCB() {
		$this->app->response->headers->set('Content-Type', 'application/json');
		$arr = [
			[
				'Number'=>'4352700607027217',
				'Type'=>'Visa',
				'Cryptogramme'=>'496',
				'Date de validataion'=>'02/2019'
			],
			[
				'Number'=>'5681832510205074',
				'Type'=>'MasterCard',
				'Cryptogramme'=>'381',
				'Date de validataion'=>'10/2020'
			],
			[
				'Number'=>'5150043873487232',
				'Type'=>'MasterCard',
				'Cryptogramme'=>'353',
				'Date de validataion'=>'10/2020'
			],
			[
				'Number'=>'4783257685706435',
				'Type'=>'Visa',
				'Cryptogramme'=>'367',
				'Date de validataion'=>'10/2019'
			],
			[
				'Number'=>'4680182543675685',
				'Type'=>'Visa',
				'Cryptogramme'=>'424',
				'Date de validataion'=>'05/2020'
			],
			[
				'Number'=>'5740571813522780',
				'Type'=>'MasterCard',
				'Cryptogramme'=>'631',
				'Date de validataion'=>'10/2020'
			],
			[
				'Number'=>'4225626631831362',
				'Type'=>'Visa',
				'Cryptogramme'=>'443',
				'Date de validataion'=>'06/2020'
			]
		];
		echo json_encode($arr);
	}
}