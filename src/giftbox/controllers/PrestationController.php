<?php
namespace giftbox\controllers;

use \giftbox\Panier;
use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\models\PrestationRating;
use Illuminate\Database\Capsule\Manager as DB;

class PrestationController extends AbstractController
{
    public function delete($id) {
		if ($this->isConnected()) {
			$id = filter_var($id, FILTER_VALIDATE_INT);
			if (($id != -1) && (is_int($id))) {
				Prestation::where('id', $id)->delete();
				$this->goBack('Prestation successfully deleted');
			} else {
				$this->goBack('Invalid prestation id');
			}
		} else {
			$this->goBack('You need to be authenticated to do this');
		}
	}
	
	public function add() {
		if (isset($_POST['submit'])) {
			$title = filter_var($_POST['title'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
			$description = filter_var($_POST['description'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
			$category = filter_var($_POST['category'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
			$price = filter_var($_POST['price'], FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
			if (($title !== null) && ($description !== null) && ($category !== null) && ($price !== null)) {
				$image = new \Bulletproof\Image($_FILES);
				if ($image['pictures']) {
					$image->setSize(1, 512000);//file size
					$image->setDimension(2000, 2000);
					$image->setLocation('web/images/thumbs');
					$upload = $image->upload();
					if ($upload) {
						$filename = substr(str_replace($image->getLocation(), '', $image->getFullPath()), 1);
						$cat = Categorie::where('id', $category)->first();
						if ($cat !== null) {
							$p = new Prestation;
							$p->nom = $title;
							$p->descr = $description;
							$p->cat_id = $category;
							$p->img = $filename;
							$p->prix = $price;
							$p->save();
							$this->goBack('Image successfully uploaded');
						} else {
							$this->goBack('Catégorie inconnue');
						}
					} else {
						$this->goBack($image['error']);
					}
				}
			} else {
				$this->goBack('Un des champs est invalide ou manquant');
			}
		}
	}
	
	public function changeVisibility($id, $visible) {
		if ($this->isConnected()) {
			$id = filter_var($id, FILTER_VALIDATE_INT);
			$visible = filter_var($visible, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
			if (($id != -1) && (is_int($id))) {
				if ($visible !== null) {
					$Presta = Prestation::where('id', $id)->first();
					$Presta->isVisible = $visible;
					$Presta->save();
					$this->goBack('Visibility changed');
				} else {
					$this->goBack('Invalid visibility parameter');
				}
			} else {
				$this->goBack('Invalid prestation id');
			}
		} else {
			$this->goBack('You need to be authenticated to do this');
		}
	}

	/*public function tri($croissant) {
		$v = new \giftbox\views\VuePrestation($this->isConnected());
        $liste = Prestation::orderBy('prix', $croissant ? 'ASC' : 'DESC')->get();
		$this->app->render('header.php');
        echo $v->render($liste, 1,$this->app);
		$this->app->render('footer.php', ['app'=>$this->app]);
    }*/
	
	private function noter($id, $note) {
		$userIP = $this->getUserIP();
		$vote = PrestationRating::where('user_ip', $userIP)->where('prestation_id', $id)->first();
		if ($vote === null) {
			$pr = new PrestationRating;
			$pr->prestation_id = $id;
			$pr->user_ip = $userIP;
			$pr->note = $note;
			$pr->save();
			$note_avg = PrestationRating::where('prestation_id', $id)->avg('note');
			$p = Prestation::where('id', $id)->first();
			$p->note = $note_avg;
			$p->save();
		} else {
			echo 'Vous avez déjà noté cette prestation.';
		}
	}
	
	public function afficherDetail($id=-1) {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		if (($id != -1) && (is_int($id))) {
			$v = new \giftbox\views\VuePrestation($this->isConnected());
			$this->app->render('header.php');
			//empecher l'acces si non visible
			$liste = Prestation::where('id', $id)->with('categorie')->first();
			$v->prestaUserNote = $liste->note;
            $v->prestAlea = $this->prestasAleatoire($id);
			echo $v->render($liste, 2, $this->app);
			$this->app->render('footer.php', ['app'=>$this->app]);
		} else {
			$this->afficherListe(-1);
		}
	}
	
	public function postNoter($id) {
		if (isset($_POST['note'])) {
			$id = filter_var($id, FILTER_VALIDATE_INT);
			$note = filter_var($_POST['note'], FILTER_VALIDATE_INT);
			$this->noter($id, $note);
		}
	}

    public function afficherListe($tri=-1, $prixMin=0, $prixMax=0) {
		$tri = filter_var($tri, FILTER_VALIDATE_INT);
		$v = new \giftbox\views\VuePrestation($this->isConnected());
		$this->app->render('header.php');
		if ($this->isConnected()) {
			$req = Prestation::with('categorie');
		} else {
			$req = Prestation::where('isVisible', 1)->with('categorie');
		}
		if (($tri != -1) && (is_int($tri))) { // TRI
			if ($tri === 0) { // Prix croissant
				$req = $req->orderBy('prix', 'ASC');
			}
			else if ($tri === 1) { // Prix décroissant
				$req = $req->orderBy('prix', 'DESC');
			}
			else if ($tri === 2) { // Ordre alphabétique
				$req = $req->orderBy('nom', 'ASC');
			}
			else if ($tri === 3) { // Appréciation
				$req = $req->orderBy('note', 'DESC');
			}
			else if ($tri === 4) { // Catégorie
				$req = $req->orderBy('cat_id', 'ASC');
			}
			else if ($tri >= 5 && $tri < 13) { // Tranches de prix (0 -> 300)
				$req = $req->whereBetween('prix', [$prixMin, $prixMax])->orderBy('prix', 'ASC');
			}
			else if ($tri === 13) { // Tranche de prix (> 300)
				$req = $req->where('prix', '>', $prixMin)->orderBy('prix', 'ASC');
			}
		} else {
			$tri = 2;
			$req = $req->orderBy('nom');
		}
		$liste = $req->get();
		$v->shoppingCart = Panier::getItems();
		echo $v->render($liste, 1, $this->app, Categorie::getAsArray(), $tri);
		$this->app->render('footer.php', ['app'=>$this->app]);
    }

    public function prestasAleatoire($id) {
        $listePresta = Prestation::where('isVisible', 1)->where('id','!=',$id)->get();
        $taille = count($listePresta)-1;
        $prestAlea1 = $listePresta[rand(0, $taille)];
        $prestAlea2 = $prestAlea1;
        while($prestAlea2 == $prestAlea1) {
            $prestAlea2 = $listePresta[rand(0, $taille)];
        }
        $prestaAlea[1] = $prestAlea1;
        $prestaAlea[2] = $prestAlea2;
        return $prestaAlea;
    }
}