<?php

namespace giftbox\controllers;

use \giftbox\models\Prestation;
use \giftbox\models\Categorie;

class CategorieController extends AbstractController
{
	public function afficherListe($id=-1) {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		$v = new \giftbox\views\VueCatalogue($this->isConnected());
		$this->app->render('header.php');
		if (($id != -1) && (is_int($id))) {
			if ($this->isConnected()) {
				$req = Prestation::where('cat_id', $id);
			} else {
				$req = Prestation::where('isVisible', 1)->where('cat_id', $id);
			}
			$liste = $req->with('categorie')->orderBy('prix')->get();
            echo $v->render($liste, 2, $this->app, Categorie::getAsArray());
		} else {
			$liste = Categorie::select()->orderBy('nom')->get();
			echo $v->render($liste, 1, $this->app);
		}
		$this->app->render('footer.php', ['app'=>$this->app]);
	}
}