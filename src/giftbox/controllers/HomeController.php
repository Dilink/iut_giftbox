<?php
namespace giftbox\controllers;

use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use Illuminate\Database\Capsule\Manager as DB;

class HomeController extends AbstractController
{
    public function home() {
		$v = new \giftbox\views\VueHome($this->isConnected());
		$this->app->render('header.php');
		echo $v->render($this->app, $this->meilleuresPrestas(), Categorie::getAsArray());
		$this->app->render('footer.php', ['app'=>$this->app]);
    }

    function meilleuresPrestas() {
        $v = new \giftbox\views\VuePrestation($this->isConnected());
		$bestPrestas = DB::select("SELECT o.* FROM `prestation` o LEFT JOIN `prestation` b ON o.cat_id = b.cat_id AND o.note < b.note WHERE b.note is NULL and o.isvisible = 1 GROUP BY cat_id ORDER BY note DESC");
		return $bestPrestas;
    }
}