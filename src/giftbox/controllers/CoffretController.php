<?php
namespace giftbox\controllers;

use \giftbox\Panier;
use \giftbox\models\Prestation;
use \giftbox\models\PrestationCoffret;
use \giftbox\models\Cagnotte;
use \giftbox\models\Categorie;
use \giftbox\models\Coffret;

class CoffretController extends AbstractController
{
	public function add($id) {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		$p = Prestation::select()->where('id', $id)->first();
		if ($p !== null) {
			if (!Panier::hasItem($id)) {
				Panier::add($id);
				$this->goBack('Prestation ajoutée avec succès.');
			} else {
				$this->goBack('Prestation déjà ajoutée !');
			}
		} else {
			$this->goBack('Prestation inconnue !');
		}
	}
	
	public function delete($id) {
		$id = filter_var($id, FILTER_VALIDATE_INT);
		$p = Prestation::select()->where('id', $id)->first();
		if ($p !== null) {
			if (Panier::hasItem($id)) {
				Panier::remove($id);
				$this->goBack('Prestation enlevée avec succès.');
			} else {
				$this->goBack('Prestation non ajoutée !');
			}
		} else {
			$this->goBack('Prestaion inconnue !');
		}
	}
	
	public function visu() {
		$v = new \giftbox\views\VueCoffret();
		$template_ShoppingCart = new \giftbox\views\templates\ShoppingCartTemplate();
		$items = Panier::getItems();
		$usedCategories = [];
		foreach ($items as $it) {
			if (count($usedCategories) > 1) {
				break;
			}
			if (!in_array($it->cat_id, $usedCategories)) {
				$usedCategories[] = $it->cat_id;
			}
		}
        
         
		$isAcceptable = ((count($items) > 1) && (count($usedCategories) > 1));
		$this->app->render('header.php');
		echo $v->renderNavbar();
		echo $template_ShoppingCart->render($items, $isAcceptable, $this->app);
		$this->app->render('footer.php', ['app'=>$this->app]);
	}
	
	/**
	 * GET http verb
	 */
	public function getInfos() {
		$v = new \giftbox\views\VueCoffret();
		$template_ShoppingCart = new \giftbox\views\templates\ShoppingCartTemplate();
		$this->app->render('header.php');
		echo $v->renderNavbar();
        //echo $v->rendergestion($this->app);
		echo $v->renderGetInfos($this->app);
		$template_ShoppingCart->renderLinkNext(false);
		echo $template_ShoppingCart->rendergestion(Panier::getItems(), true, $this->app);
		$this->app->render('footer.php', ['app'=>$this->app]);
	}
	
	/**
	 * POST http verb
	 */
	public function postRecap() {
        if(isset($_POST['email']) && isset($_POST['firstname'])  && isset($_POST['lastname'])  && isset($_POST['message'])){
			$email = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
			$nom = filter_var($_POST["lastname"], FILTER_SANITIZE_STRING);
			$prenom = filter_var($_POST["firstname"], FILTER_SANITIZE_STRING);
			if (isset($_POST["pass"])) {
				$password = filter_var($_POST["pass"], FILTER_VALIDATE_REGEXP, array( "options"=> array( "regexp" => "/.{6,25}/")) );
			}
			$message= filter_var($_POST["message"], FILTER_SANITIZE_STRING);
			$paiement= filter_var($_POST["paiement"], FILTER_SANITIZE_STRING);
			$coffret = new Coffret;
			$coffret->nom=$nom;
			$coffret->prenom=$prenom;
			$coffret->email=$email;
			$coffret->password = (isset($password)) ? password_hash($password, PASSWORD_DEFAULT, ['cost'=>12]) : null ;
			$coffret->message=$message;
			$coffret->type_paiement = ($paiement == 'cagnotte') ? 'cagnotte' : 'classique';
			$coffret->urlgestion=sha1(uniqid());
			$coffret->save();
			
			$v = new \giftbox\views\VueCoffret();
			$viewEmail = new \giftbox\views\templates\VueEmail();
			$this->app->render('header.php');
			echo $v->renderNavbar();
            
            $panier = Panier::getItems();
            
            foreach($panier as $p){
                 $prestacof= new PrestationCoffret;
                 $prestacof->prestation_id=$p->id;
                 $prestacof->coffret_id=$coffret->id;
                 $prestacof->save();
            }
        
			if ($paiement === 'cagnotte') {
				$cagnotte = new Cagnotte;
				$cagnotte->url=sha1(uniqid());
				$cagnotte->montant = Panier::getMontant();
				$cagnotte->idcoffret = $coffret->id;
				$cagnotte->save();
				$tabinfo = array($email, $nom, $prenom, password_hash($password, PASSWORD_DEFAULT, ['cost'=>12]), $paiement, $cagnotte->url, $coffret->urlgestion);
				$_SESSION['infocoffret']=$tabinfo;
				echo $viewEmail->render($coffret->urlgestion, $this->app, $cagnotte->url);
			} else if ($paiement === 'classic') {
				$_SESSION['coffret_id'] = $coffret->id;
				$_SESSION['coffret_idgestion'] = $coffret->urlgestion;
				echo $v->renderPaymentClassic($this->app);
			}
			
			$this->app->render('footer.php', ['app'=>$this->app]);
            
           // var_dump($_SESSION['infocoffret']);
          // $cagnotte::cloture(69);
          //    echo $this->afficheEtat(72);
          //  echo $this->genererUrlCadeaux(72);
           // echo $this->afficheEtat(72);
            
	}
}
    
    
    
    /**
    * POST
    */
    public function modifiermdp($token){
      $urlverif=filter_var($token, FILTER_SANITIZE_STRING);
      $coffret=Coffret::where('urlgestion',$urlverif)->first();
      $password=$coffret->password;
      echo $password;
      $pass=password_hash('canard',PASSWORD_BCRYPT);
      if(password_verify($_POST['passcheck'],$password)){
          echo'verifier';
          if ($_POST['passnew']===$_POST['passnewcheck']){    
          $coffret->password=password_hash($_POST['passnew'],PASSWORD_BCRYPT);
          $coffret->save();
          }
          else{
              echo 'Mot de passe mauvais lel';
          }
      }
         $this->app->response->redirect($this->app->urlFor('coffretGestion', ['token'=>$token]), 303);
    }
    
    public function afficheEtat($id){
        $cagnotte=$cagnotte=Cagnotte::where('id',$id)->first();
        $coffret=Coffret::where('id',$cagnotte->idcoffret)->first();
        return $coffret->etat;
    }
    
    public function genererUrlClassique($id){
        $coffret=Coffret::where('id',$id)->first();
        $coffret->paye=1;
        $coffret->save();
    }
    
    public static function genererUrlCagnotte($id) {
        $cagnotte=Cagnotte::where('id',$id)->first();
        $coffret=Coffret::where('id',$cagnotte->idcoffret)->first();
        if ($cagnotte->cloturer ==1){
            $coffret->urlcadeaux=sha1(uniqid());
            $coffret->etat="En attente d'ouverture";
            $coffret->save();
        }
        return $coffret->urlcadeaux;
	}
	
	public function gestionGet($token) {
		$token = filter_var($token, FILTER_SANITIZE_STRING);
        $c=Coffret::where('urlgestion',$token)->first();
        $cagnotte=Cagnotte::where('idcoffret',$c->id)->first();
    
		if (!isset($_SESSION['canAccessCoffretGestion'])) {
        $v = new \giftbox\views\VueCoffret();
		$template_ShoppingCart = new \giftbox\views\templates\ShoppingCartTemplate();
		$this->app->render('header.php');
		echo $v->renderNavbar();
        echo $v->renderGestionSession($token,$this->app, $c->etat);
		$this->app->render('footer.php', ['app'=>$this->app]);
			return;
		}
        else{
             $viewEmail = new \giftbox\views\templates\VueEmail();
             $v = new \giftbox\views\VueCoffret();
		$template_ShoppingCart = new \giftbox\views\templates\ShoppingCartTemplate();
		$this->app->render('header.php');
		echo $v->renderNavbar();
        echo $v->rendergestion($token,$this->app,$c->etat);
        if(isset($cagnotte)){
             echo $viewEmail->renderCagnotte($this->app,$cagnotte->url);
        }
		//echo $v->renderGetInfos($this->app);
		$template_ShoppingCart->renderLinkNext(false);
		echo $template_ShoppingCart->rendergestion(Panier::getItems(), true, $this->app);
		$this->app->render('footer.php', ['app'=>$this->app]);
			return;
        }
        
        
	}
	
    
    
	public function gestionPost($token) {
        
		$token = filter_var($token, FILTER_SANITIZE_STRING);

		$cof = Coffret::where('urlgestion', $token)->first();
		if ($cof === null) {
          
			$this->addError('error_1', 'The token is owned by nothing');
			$this->goBack('The token is owned by nothing');
			return;
		}
        
		if (isset($_POST['submit-askaccess'])) {
            
            
			if (isset($_SESSION['canAccessCoffretGestion'])) {
			} else {
				$passCoffret = $cof->password;
				if (password_verify($_POST['passAccess'], $passCoffret)) {
						$_SESSION['canAccessCoffretGestion'] = true;
                    $this->app->response->redirect($this->app->urlFor('coffretGestion'), 303);
				} else {
					$this->addError('wrongpassword', 'The password is wrong');
					$this->goBack('The password is wrong');
				}
			}
		}
		else if (isset($_POST['submit-newpasswd']) && isset($_SESSION['canAccessCoffretGestion'])) {
   
			$this->modifiermdp($token);
		}
	}
	
	/**
	 * POST http verb
	 */
	public function validation() {
		if (isset($_POST['submit-classic']) && isset($_SESSION['coffret_id'])) {
			$moneyCard = $_POST['card'];
			$mc_type = filter_var($moneyCard['type'], FILTER_SANITIZE_STRING);
			$mc_number = filter_var($moneyCard['number'], FILTER_VALIDATE_INT);
			$mc_cvc = filter_var($moneyCard['cvc'], FILTER_VALIDATE_INT);
			$mc_expm = filter_var($moneyCard['expire-month'], FILTER_SANITIZE_STRING);
			$mc_expy = filter_var($moneyCard['expire-year'], FILTER_SANITIZE_STRING);
			$card = \Inacho\CreditCard::validCreditCard($moneyCard['number'], $moneyCard['type']);
			if ($card['valid']) {
				$validCvc = \Inacho\CreditCard::validCvc($mc_cvc, $card['type']);
				if ($validCvc) {
					$validDate = \Inacho\CreditCard::validDate($mc_expy, $mc_expm); // past date
					if ($validDate) {
						$this->genererUrlClassique($_SESSION['coffret_id']);
						$this->addSuccess('ccvalid', 'Credit card is valid.');
//						var_dump('Credit card is valid.');
						$this->addSuccess('clickredirect1', "Vous pouvez cliquer <a href=\"{$this->app->urlFor('coffretGestion', ['token'=>$_SESSION['coffret_idgestion']])}\">ici pour la gestion</a> du coffret.<br/>");
//						$this->goBack('Credit card is valid.');
					} else {
						$this->addError('expidateinvalid', 'Expiration date is invalid !');
//						var_dump('Expiration date is invalid !');
//						$this->goBack('Expiration date is invalid !');
					}
				} else {
					$this->addError('cvcinvalid', 'CVC is invalid !');
//					var_dump('CVC is invalid !');
//					$this->goBack('CVC is invalid !');
				}
			} else {
				$this->addError('ccinvalid', 'Credit card is invalid !');
//				var_dump('Credit card is invalid !');
//				$this->goBack('Credit card is invalid !');
			}
			
			
			var_dump($this->getMessages());
			var_dump($this->getSuccessMessages());
			var_dump($this->getInfoMessages());
			var_dump($this->getErrorMessages());
			
		}
	}
	
	public function discoverAll($url) {
		$url = filter_var($url, FILTER_SANITIZE_STRING);
		$cagnotte = Cagnotte::where('url', $url)->first();
		if ($cagnotte !== null) {
			$v = new \giftbox\views\VueCoffret();
			$liste = PrestationCoffret::where('coffret_id', $cagnotte->idcoffret)->with('prestations')->get();
			$coffret = Coffret::where('id', $cagnotte->idcoffret)->first();
            $coffret->etat='Ouvert';
            $coffret->save();
			if ($coffret !== null) {
				$this->app->render('header.php');
				echo $v->renderNavbar();
				echo $v->renderDiscoverAll($coffret, $liste, Categorie::getAsArray(), $this->app);
				$this->app->render('footer.php', ['app'=>$this->app]);
			} else {
				echo 'Le coffret est introuvable.';
			}
		} else {
			echo 'Cette cagnotte n\'existe pas.';
		}
	}

	
	public function discoverSingle($url, $pid=0) {
		$url = filter_var($url, FILTER_SANITIZE_STRING);
		$pid = filter_var($pid, FILTER_VALIDATE_INT);
		if (!(($pid != -1) && is_int($pid) && ($pid > 0))) {
			$pid = 0;
		}
		$cagnotte = Cagnotte::where('url', $url)->first();
		$this->app->render('header.php');
		$v = new \giftbox\views\VueCoffret();
		echo $v->renderNavbar();
		if ($cagnotte !== null) {
			$coffret = Coffret::where('id', $cagnotte->idcoffret)->first();
               $coffret->etat='Ouvert';
            $coffret->save();
			if ($coffret !== null) {
				$prestaCoffret = PrestationCoffret::where('coffret_id', $cagnotte->idcoffret)->take(1)->skip($pid)->first();
				if ($prestaCoffret !== null) {
					$presta = Prestation::where('id', $prestaCoffret->prestation_id)->first();
					if ($presta !== null) {
						echo $v->renderDiscoverSingle($url, $coffret, $presta, Categorie::getAsArray(), $this->app, $pid+1);
					} else {
						echo '<section><h3 style="padding:3em 0;text-align:center;background-color:#ccc;">Auncune prestation n\'a été trouvée.</h3></section>';
					}
				} else {
					echo '<section><h3 style="padding:3em 0;text-align:center;background-color:#ccc;">Auncune prestation n\'a été trouvée.</h3></section>';
				}
			} else {
				echo '<section><h3 style="padding:3em 0;text-align:center;background-color:#ccc;">Le coffret est introuvable.</h3></section>';
			}
		} else {
			echo '<section><h3 style="padding:3em 0;text-align:center;background-color:#ccc;">Cette cagnotte n\'existe pas.</h3></section>';
		}
		$this->app->render('footer.php', ['app'=>$this->app]);
	}
}