<?php
namespace giftbox\controllers;

class Router
{
	private $app;

	public function __construct($app) {
		$this->app = $app;
	}

	public function call($url, $action, $method) {
		return $this->app->$method($url, function () use ($action) {
			$action = explode('@',$action);
			$controllername = '\giftbox\controllers\\' . $action[0] . 'Controller';
			$method = $action[1];
			if (class_exists($controllername)) {
				$controller = new $controllername();
			} else {
				$controller = new PrestationController();
				$method = 'liste';
			}
			// On appelle la methode du controller avec d'éventuels paramètres.
			call_user_func_array([$controller, $method], func_get_args());
		});
	}
	
	public function get($url, $action) {
		return $this->call($url, $action, 'get');
	}
	
	public function post($url, $action) {
		return $this->call($url, $action, 'post');
	}
}