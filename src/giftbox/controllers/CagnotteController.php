<?php
namespace giftbox\controllers;

use \giftbox\models\Cagnotte;
use \giftbox\models\Categorie;
use \giftbox\models\Coffret;
use giftbox\models\PrestationCoffret;

class CagnotteController extends AbstractController
{
    public function afficheURL($id){
        return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$this->app->urlFor('cagnotte',['id'=>$id]);                                                   
    }
	
	public function removeAccess() {
		unset($_SESSION['canAccessCoffretGestion']);
		$this->goBack('Authorisation de gestion du coffret retirée');
	}

    public function visu($url) {
		$url = filter_var($url, FILTER_SANITIZE_STRING);
		$this->visuCagnotte($url, isset($_SESSION['canAccessCoffretGestion']));
    }
	
	private function visuCagnotte($url, $isAdmin) {
        $v = new \giftbox\views\VueCagnotte();
        $cagnotte = Cagnotte::where('url', $url)->first();
		$this->app->render('header.php');
		echo $v->renderNavbar();
		if ($cagnotte !== null) {
			$urlCadeau = '';
			if ($isAdmin) {
				$urlCadeau = CoffretController::genererUrlCagnotte($cagnotte->id);
			}
			$liste = PrestationCoffret::where('coffret_id', $cagnotte->idcoffret)->with('prestations')->get();
			echo $v->render($url, $cagnotte, $liste, Categorie::getAsArray(), $this->app, $isAdmin, $urlCadeau);
		}
		$this->app->render('footer.php', ['app'=>$this->app]);
	}
	
	public function close($url) {
		if (isset($_SESSION['canAccessCoffretGestion'])) {
			$url = filter_var($url, FILTER_SANITIZE_STRING);
			$cagnotte = Cagnotte::where('url', $url)->first();
			var_dump($cagnotte->idcoffret);
			$coffret = Coffret::where('id',$cagnotte->idcoffret)->first();
			if ($cagnotte->payer >= $cagnotte->montant) {
				$cagnotte->cloturer = 1;
				$cagnotte->save();
				$coffret->etat="Financé , en attente d'ouverture";
				$coffret->save();
			}
			$this->goBack('OK');
		} else {
			$this->goBack('Errorg');
		}
	}

    public function generateID(){
        return sha1(uniqid());
    }
    
    
    public function addCagnotte($montant){
        $url=$this->generateID();
        $cagnotte= new Cagnotte;
        $cagnotte->url = $url;
        $cagnotte->montant=$montant;
        $cagnotte->save();
    }
    
    private function payer($url, $paye) {
        $cagnotte = Cagnotte::where('url', $url)->first();
        if (($paye > 0) && ($cagnotte->payer < $cagnotte->montant)) {
            $cagnotte->payer += $paye;
            $cagnotte->save();
        } else {
            //echo 'Montant incorrect';
			$this->addError('invalidpaye', 'Montant incorrect');
        }
    }  

    public function payerPost($url) {
		$url = filter_var($url, FILTER_SANITIZE_STRING);
        if (isset($_POST['montant'])) {
			$_POST['montant'] = filter_var($_POST['montant'], FILTER_VALIDATE_FLOAT);
            $this->payer($url, $_POST['montant']);
            $this->visu($url);
        }
    }
}