<?php
require_once  'vendor/autoload.php';
session_start();
\giftbox\Panier::read();

define('URL_ROOT', substr($_SERVER['SCRIPT_NAME'], 0, -9));
define('URL_CSS', URL_ROOT.'web/css/');
define('URL_JS', URL_ROOT.'web/js/');
define('URL_IMAGES', URL_ROOT.'web/images/');

function initMenuAccess($app) {
	global $MENU;
	$MENU = array(
		'home' => $app->urlFor('home'),
		'prestations' => $app->urlFor('prestations'),
		'categories' => $app->urlFor('categories'),
		'coffret' => $app->urlFor('coffretVisu')
	);
}

use Illuminate\Database\Capsule\Manager as DB;
use giftbox\controllers\Router;

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/db.config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
 * Initialisation de Slim
 */
$configuration = [
    'settings' => [
        'displayErrorDetails' => true
    ],
    'templates.path' => 'src/giftbox/views/templates'
];

$app = new \Slim\Slim($configuration);
//Routeur
$router = new Router($app);
//Création des routes

$router->get('/', 'Home@home')->name('home');
$router->get('/prestations(/)', 'Prestation@afficherListe')->name('prestations');
$router->get('/prestations/tri/:tri(/:prixmin/:prixmax(/))', 'Prestation@afficherListe')->name('prestationsTri');
$router->get('/prestation/:id(/)', 'Prestation@afficherDetail')->name('prestation');
$router->post('/prestation/:id/notation(/)', 'Prestation@postNoter')->name('prestationNotation');
$router->get('/prestation/visibility/:id/:visible(/)', 'Prestation@changeVisibility')->name('prestationVisibility');
$router->get('/prestation/delete/:id(/)', 'Prestation@delete')->name('prestationDelete');
$router->post('/prestations/add(/)', 'Prestation@add')->name('prestationAdd');
$router->get('/categories(/)', 'Categorie@afficherListe')->name('categories');
$router->get('/categories/:id(/)', 'Categorie@afficherListe')->name('categorie');
$router->get('/login(/)', 'Login@loginGet')->name('login');
$router->post('/login(/)', 'Login@loginPost');
$router->get('/logout(/)', 'Login@logout')->name('logout');
$router->get('/coffret(/)', 'Coffret@visu')->name('coffretVisu');
$router->get('/coffret/add/:id(/)', 'Coffret@add')->name('coffretAdd');
$router->get('/coffret/del/:id(/)', 'Coffret@delete')->name('coffretDelete');
$router->get('/coffret/gestion/:token(/)', 'Coffret@gestionGet')->name('coffretGestion');
$router->post('/coffret/gestion/:token(/)', 'Coffret@gestionPost');

/*$router->get('/cagnotte/gestion/:url(/)', 'Cagnotte@gestion')->name('cagnotteGestion');
$router->get('/cagnotte/participation/:url(/)', 'Cagnotte@participation')->name('cagnotteParticipation');*/
$router->get('/cadeaux/:url(/)', 'Coffret@discoverAll')->name('coffretDiscoverAll');
$router->get('/cadeau/:url(/:pid)(/)', 'Coffret@discoverSingle')->name('coffretDiscoverSingle');

$router->get('/coffret/informations(/)', 'Coffret@getInfos')->name('coffretInfos');
$router->post('/coffret/informations(/)', 'Coffret@postRecap')->name('coffretRecap');
$router->post('/coffret/validation(/)', 'Coffret@validation')->name('coffretValidation');

$router->get('/cagnotte/:url(/)','Cagnotte@visu')->name('cagnotteVisu');
$router->post('/cagnotte/:url(/)','Cagnotte@payerPost');
$router->get('/cagnotte/close/:url(/)','Cagnotte@close')->name('cagnotteClose');
$router->get('/cagnotte/remove-access(/)', 'Cagnotte@removeAccess')->name('logoutCoffretGestion');

$router->get('/debug/credit-card(/)', 'Debug@generateCB')->name('debugCB');

initMenuAccess($app);
$app->run();
/*
http://semantic-ui.com/examples/fixed.html
http://semantic-ui.com/examples/homepage.html
http://semantic-ui.com/examples/login.html
http://semantic-ui.com/examples/theming.html#iFrameResizer5

http://semantic-ui.com/views/statistic.html
http://semantic-ui.com/views/card.html#link-card
http://semantic-ui.com/views/comment.html

http://semantic-ui.com/elements/label.html#tag
*/