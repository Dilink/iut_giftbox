jQuery('document').ready(function() {
	var $ = jQuery;
	$('.special.cards .image').dimmer({ on: 'hover'	});
//	$('.card .dimmer').dimmer({ on: 'hover' });
	$('.star.rating').rating({ maxRating: 5 });
	$('.star.rating').rating('disable');
	$('.ui.dropdown').dropdown();
	$('.shopping-cart-btn').click(function() {
		$(this).parent().toggleClass('open');
	});
	$('.ui.radio.checkbox').checkbox();
	$('.ui.progress').progress();
	new Clipboard('.clipboard');
	//var clipboardDemos=new Clipboard('.clipboard');clipboardDemos.on('success',function(e){e.clearSelection();console.info('Action:',e.action);console.info('Text:',e.text);console.info('Trigger:',e.trigger);showTooltip(e.trigger,'Copied!');});clipboardDemos.on('error',function(e){console.error('Action:',e.action);console.error('Trigger:',e.trigger);showTooltip(e.trigger,fallbackMessage(e.action));});
	if ((typeof notationURL !== 'undefined') && (notationURL !== '')) {
		$('.star.rating.editable').rating({
			onRate: function(note) {
				var self = $(this);
				$.post(notationURL, {note: note}, function() {
					self.rating('disable');
				});
			}
		});
	}

	$('.masthead').visibility({
		once: false,
		onBottomPassed: function() {
			$('.fixed.menuc').transition('fade in');
		},
		onBottomPassedReverse: function() {
			$('.fixed.menuc').transition('fade out');
		}
	});
    
    $('.ui.form').form({
        fields: {
            email: {
                identifier  : 'email',
                rules: [{
                        type   : 'empty',
                        prompt : 'Please enter your e-mail'
                    }, {
                        type   : 'email',
                        prompt : 'Please enter a valid e-mail'
                    }
                ]
            },
            password: {
                identifier  : 'password',
                rules: [{
                        type   : 'empty',
                        prompt : 'Please enter your password'
                    }, {
                        type   : 'length[6]',
                        prompt : 'Your password must be at least 6 characters'
                    }
                ]
            }
        }
    });

	// Automatically shows on init if cookie isnt set | using cookies message
	$('.cookie.nag').nag({
		key: 'accepts-cookies',
		value: true
	});

	setInterval(function() {
		$('.shape.text').shape('flip up');//right
	}, 3500);
	
	// forEach polyfill
	Array.prototype.forEach||(Array.prototype.forEach=function(a,b){var c,d;if(null===this)throw new TypeError("this is null or n is not defined");var e=Object(this),f=e.length>>>0;if("function"!=typeof a)throw new TypeError(a+" n is not a function");for(arguments.length>1&&(c=b),d=0;d<f;){var g;d in e&&(g=e[d],a.call(c,g,d,e)),d++}});
	
	var inputs = document.querySelectorAll('.inputfile');
	Array.prototype.forEach.call(inputs, function(input) {
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;
		input.addEventListener( 'change', function(e) {
			var fileName = '';
			if (this.files && this.files.length > 1) {
				fileName = (this.getAttribute('data-multiple-caption' ) || '').replace('{count}', this.files.length);
			} else {
				fileName = e.target.value.split( '\\' ).pop();
			}
			if (fileName) {
				label.querySelector('span').innerHTML = fileName;
			} else {
				label.innerHTML = labelVal;
			}
		});
	});
	
	$('#fileupload').change(function(e) {
		if ('FileReader' in window) {
			var iup = $('#image-upload-preview');
			if (this.files && this.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					iup.attr('src', e.target.result).css('display', 'block');
				};
				reader.readAsDataURL(this.files[0]);
			}
		}
	});
});